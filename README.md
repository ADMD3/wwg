# Wer wird Gillionär


## Getting started

Dies ist eine kleine implementation und adaption des beliebten Quizspiels "WWM".

Über Konsole folgendes ausführen:

```
npm install
npm run dev
```

## Eigene Quizes

Eigene Quizes können derzeit in das Quizregister (Record<string, Quiz>) geladen werden. Die entsprechenden Typen befinden sich in types.ts.

Man beachte, dass das Layout derzeit keine breakpoints hat, von der Form her verscheidene Quizes oder Fragen also das Layout brechen können.

