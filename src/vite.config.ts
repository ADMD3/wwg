import { defineConfig } from 'vite';
import preact from '@preact/preset-vite';

/// <reference types="vite-plugin-svgr/client" />

//SVG:
//https://github.com/pd4d10/vite-plugin-svgr
import svgr from "vite-plugin-svgr";

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [preact(), svgr()],
});

