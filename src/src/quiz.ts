import { demoQuiz, testAQuiz, testBQuiz } from "./data";
import { Quiz } from "./model/types";


export const QuizData: Record<string, Quiz> = {
    "Demo": demoQuiz(),
    "A": testAQuiz(),
    "B": testBQuiz(),
}