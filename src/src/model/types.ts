

export type QuizState = {
    note: string;

    time: Date;

    quiz: Quiz|string;
    stage: number;
}

export type Quiz = {
    title: string;
    Stages: QuizStage[];
}

export type QuizStage = {
    isHidden?: boolean;
    reward: number|string;
    Question: Question;
}

export type Question = {
    Content: any;
    Question: string;

    Answers: [ a: Answer, b: Answer, c: Answer, d: Answer,]
};

export type Answer = {
    text: string;
    correct: boolean;
}




