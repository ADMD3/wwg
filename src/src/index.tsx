import { render } from 'preact';

import './style.css';
import { Button, ButtonGroup, Grid, IconButton, Stack, styled } from '@mui/joy';
import Sheet from '@mui/joy/Sheet';
import { Quiz, } from './model/types';
import { StateUpdater, useCallback, useState } from 'preact/hooks';
import { Children } from 'preact/compat';
//import  SVGComponent from './logo';

import RestartAltIcon from '@mui/icons-material/RestartAlt';
import SwapCallsIcon from '@mui/icons-material/SwapCalls';

import "./logo.css";
import Logo from "./assets/gillionär/gillionär-frei.svg?react";

import { demoQuiz } from './data';
import { QuizData } from './quiz';
import { Dialog, DialogTitle, List, ListItem, ListItemButton, ListItemText } from '@mui/material';



type State = {
	quizId: string;
	currentStage: number;

	gameOver: boolean; //blocks all interaction, except for resets
	gameWon: boolean; //if game is over but  not by answering wrong

	currentSelect: number; //currently selected asnwer
	commited: boolean;

	//Jokers
	jokers: [ fifty: boolean, phone: boolean, checkpoint: boolean ];
	bannedAnswers: number[]; //50/50 joker!
	savedIndex: number; //Save crystal
}

export function App() {

	const [q, setQ] = useState<State>({
			quizId: 'Demo',
			currentStage: 0,

			gameOver: false,
			gameWon: false,
			
			currentSelect: -1,
			commited: false,

			jokers: [true, true, true],
			bannedAnswers: [],
			savedIndex: -1,
	})

	const quiz = QuizData[q.quizId] ?? demoQuiz();

	const currentStage = quiz.Stages[Math.min(q.currentStage, quiz.Stages.length-1)];
	const question = currentStage.Question;

	let WonReward: string|number = "";
	if (q.currentStage == 0) {
		WonReward = "Für nix gibst NIX!";
	} else if (q.currentStage === quiz.Stages.length) {
		WonReward = quiz.Stages.at(-1).reward
	} else if (q.gameOver) {
		
		const save = q.savedIndex;
		if (q.gameWon) {
			WonReward = quiz.Stages[Math.max(q.currentStage-1,0)].reward;
		} else if (save  >= 0) {
			WonReward = quiz.Stages[save].reward;
		} else {
			WonReward = "Es gibt NIX!";
		}
	} else {
		WonReward = quiz.Stages[Math.max(q.currentStage-1,0)].reward
	}
	
	
	const answer = useCallback((answer: number) => {
		if (q.commited && !q.gameOver) {
			if (question.Answers[q.currentSelect].correct) {
				if (quiz.Stages.length-1 === q.currentStage) {
					setQ({
						...q,
						currentStage: quiz.Stages.length,
						gameOver: true,
						gameWon: true,
					})
				} else {
					//iterate to next stage
					setQ({
						...q,
						currentStage: q.currentStage+1,
						currentSelect: -1,
						commited: false,
						bannedAnswers: [],
					})
				}
			} else {
				//game over if bad answer
				setQ({
					...q,
					gameOver: true,
				})
			}
		} else {
			setQ({
				...q,
				currentSelect: answer,
				commited: answer === q.currentSelect
			})
		}

	  }, [q]); 

	const reset = useCallback(() => {
			setQ({
				quizId: q.quizId, //just carry over this one here
				currentStage: 0,

				gameOver: false,
				gameWon: false,
				
				currentSelect: -1,
				commited: false,

				jokers: [true, true, true],
				bannedAnswers: [],
				savedIndex: -1,
		})
	}, []);

	return (
		<div style=" 

		">
			<div style="display: flex; gap: 1em">		
				<div style="flex-grow: 3">
					<div style="margin: 0.5em 0em; min-height: 5em">
						<Logo class="logo"></Logo>
					</div>
					<div>
						<Grid 
							container rowSpacing={1}
							columnSpacing={{ xs: 1, sm: 2, md: 3 }}
							sx={{ width: '100%' }}
						 >
							<Grid xs={12}>
								{/* Debug: Answered: {String(isAnswered)}; Choosen Answer: {choosen} */}
								<div class="question-element question-container">
									{q.gameWon ? `Gewonnen! ${WonReward}` : 
									q.gameOver ? `Game Over! ${WonReward}` : question.Question}
								</div>

							</Grid>
							{question.Answers.map((value, i) =>
								<Grid xs={6}>
									<AnswerOption onClick={() => answer(i)} option={value} reveal={q.commited} choosen={q.currentSelect == i}>
										{ q.bannedAnswers.includes(i) ? "" : value.text}
									</AnswerOption>
								</Grid>
							)}
						</Grid>
					</div>
				</div>
				<Stage quiz={quiz} current={q.currentStage} state={q} setState={setQ}></Stage>
			</div>
			
		</div>
	);
}


const Item = styled(Sheet)(({ theme }) => ({
	backgroundColor:
	  theme.palette.mode === 'dark' ? theme.palette.background.level1 : '#fff',
	...theme.typography['body-sm'],
	padding: theme.spacing(1),
	textAlign: 'center',
	borderRadius: 4,
	color: theme.vars.palette.text.secondary,
  }));

  const AnswerButton = (props) => { return (
	<Item>{props.text}</Item>
  ) 
};

const AnswerOption = (props) => {
	let children = props.children;
	console.log(props); //class={"answer-container"}

	return (
	<div onClick={props.onClick}  class={`answer-container question-element
		${props.reveal ? "reveal" : ""} 
		${props.option.correct ? "correct" : "false"} 
		${props.choosen ? "choosen" : ""}
		`}>
		{Children.map(children, (c,) => c)}
	</div>);
}


function Stage(props: { quiz: Quiz, current: number, state: State, setState: StateUpdater<State>}) {
	console.log(props.quiz, props.current)

	const reset = useCallback(() => {
		props.setState({
			quizId: props.state.quizId, //just carry over this one here
			currentStage: 0,

			gameOver: false,
			gameWon: false,
			
			currentSelect: -1,
			commited: false,

			jokers: [true, true, true],
			bannedAnswers: [],
			savedIndex: -1,
	})
	}, []);


	const optOut = () => {
		props.setState({
			...props.state,
			gameOver: true,
			gameWon: true
		})
	}


	const fiftyJoker = () => {
		//Get 50% of all answers
		const answers = props.quiz.Stages[props.state.currentStage].Question.Answers;
		let validWithin = false;
		const workarr = answers.map( (a,i) => i)
		const shuffled = workarr.sort(() => 0.5 - Math.random());

		const targetLength = Math.ceil(answers.length / 2);

		const removeAnswers = [];
		for (let i of shuffled) {
			if (removeAnswers.length == targetLength) break; //we finished
			if (!validWithin && answers[i].correct ) { //skip answer
				validWithin = true;
				continue;
			} else {
				removeAnswers.push(i);
			}
		}

		props.setState({
			...props.state,
			jokers: [ false,  props.state.jokers[1],  props.state.jokers[2]],
			bannedAnswers: removeAnswers,
		});
	}

	const phoneJoker = () => {
		props.setState({
			...props.state,
			jokers: [ props.state.jokers[0], false,  props.state.jokers[2]]
		});
	}

	const saveJoker = () => {
		props.setState({
			...props.state,
			jokers: [ props.state.jokers[0], props.state.jokers[1], false],
			savedIndex: props.state.commited ? props.state.currentStage : Math.max(props.state.currentStage-1, 0)
		});
	}

	const [ dialogOpen, setDialogOpen] = useState(false);
	
	const  handleQuizSelectClose = (value: string) => {
		setDialogOpen(false);
		//setSelectedValue(value);
		props.setState({
			quizId: value, //just carry over this one here
			currentStage: 0,

			gameOver: false,
			gameWon: false,
			
			currentSelect: -1,
			commited: false,

			jokers: [true, true, true],
			bannedAnswers: [],
			savedIndex: -1,
	})
	};


	return (
	<Stack flex={"flex"} flexDirection={"column"} class={"text-white"} gap={2} sx={{ paddingTop: "10em"}}>
		<>
		{/* Joker */}
		<Stack flexDirection={"row"} gap={2}>
			<Button disabled={!props.state.jokers[0]} onClick={() => fiftyJoker()}>50/50</Button>
			<Button disabled={!props.state.jokers[2]} onClick={() => saveJoker()}>Speicher-Kristall</Button>
			<Button disabled={!props.state.jokers[1]} onClick={() => phoneJoker()}>Kontakt-Perle</Button>
		</Stack>
		<div style="display: flex; flex-direction: column-reverse; flex-grow: 1; gap: 2em; font-size: 14pt;" class={"text-white "}>
			{props.quiz.Stages.map( (item, i) => {

				return <p style={i == props.current ? "background-color: #ca8a04; text-decoration: underline" : ""}>{(i > props.current && item.isHidden) ? "???" : item.reward} { i == props.state.savedIndex ? "!!" : ""}</p>
			})}
		</div>
		{/* Joker */}
		<div>
			<ButtonGroup>
				{/* Reset */}
				<IconButton onClick={() => reset()}><RestartAltIcon/></IconButton>
				<IconButton  onClick={() => setDialogOpen(true)}><SwapCallsIcon/></IconButton>
				<IconButton onClick={() => optOut()}>O</IconButton>
			</ButtonGroup>
		</div>
		{/* @ts-ignore */}
		<SelectQuizDialog
		        //selectedValue={selectedValue}
				open={dialogOpen}
				onClose={handleQuizSelectClose}
				list={[ ...Object.keys(QuizData)]}
		></SelectQuizDialog>
		</>
	</Stack>
	);
}

export interface SimpleDialogProps {
	open: boolean;
	//selectedValue: string;
	onClose: (value: string) => void;

	list: string[];
}
function SelectQuizDialog(props: SimpleDialogProps) {
	const { onClose, open } = props;
  
	const handleClose = () => {

	};
  
	const handleListItemClick = (value: string) => {
	  onClose(value);
	};
  
	return (
	  <Dialog onClose={handleClose} open={open}>
		<DialogTitle>Restart with Quiz:</DialogTitle>
		<List sx={{ pt: 0 }}>
		  {props.list.map((email) => (
			<ListItem disableGutters key={email}>
			  <ListItemButton onClick={() => handleListItemClick(email)}>
				<ListItemText primary={email} />
			  </ListItemButton>
			</ListItem>
		  ))}
		</List>
	  </Dialog>
	);
  }


render(<App />, document.getElementById('app'));
