import { Title } from "@mui/icons-material";
import { Question, Quiz, QuizStage } from "./model/types";


export const demoQuestion: Question = {
    Content: "",
    Question: "Who is Chrestchen McNiel?",
    Answers: [
        { text: "The VulnStack master", correct: true},
        { text: "A leading member of the fc 'OP'", correct: true},
        { text: "Member of the eternal potato circle", correct: false},
        { text: "Chrestchen McNiel", correct: true},
    ]
}

export function demoQuiz(): Quiz {
    //now as a "start page"
    const stages: QuizStage[] = [
        {
            reward: "Start",
            Question: {
                Content: "",
                Question: "Willkommen!",
                Answers: [
                    { text: "Viel Spaß", correct: true},
                    { text: "Viel Gil", correct: true},
                    { text: "Viel Wissen", correct: false},
                    { text: "Viel Glück", correct: true},
                ]
            }
        },
   ];


    return {
        title: "Demo",
        Stages: stages
    }
}



//////

export function testAQuiz() : Quiz {
    let q: Quiz = {
        title: "",
        Stages: [ 
            {
                reward: "1 Gil",
                Question: {
                    Content: "",
                    Question: "Wie heißt das Wohngebiet von Limsa Lominsa?",
                    Answers: [
                        { text: "Lavendelbeete", correct: false},
                        { text: "Sonnenküste", correct: false},
                        { text: "Kelchkuppe", correct: false},
                        { text: "Dorf des Nebels", correct: true},
                    ]
                }
            },
            {
                reward: "5.000 Gil",
                Question: {
                    Content: "",
                    Question: "In welchem Stadtstaat startet man mit der Klasse Waldläufer?",
                    Answers: [                  
                        { text: "Ul'dah", correct: false},
                        { text: "Gridania", correct: true},
                        { text: "Gold Saucer", correct: false},
                        { text: "Limsa Lominsa", correct: false},
                    ]
                }
            },
            {
                reward: "10.000 Gil",
                Question: {
                    Content: "",
                    Question: "Wie heißt die Währung im Gold Saucer?",
                    Answers: [
                        { text: "Multi Glückspunkte", correct: false},
                        { text: "Merlwybs Gin Punkte", correct: false},
                        { text: "Manderville Gold Saucer-Punkte", correct: true},
                        { text: "Moneten Gil Punkte", correct: false},
                    ]
                }
            },
            {
                reward: "25.000 Gil",
                Question: {
                    Content: "",
                    Question: "Mit was ruft man den Chocobo an seine Seite?",
                    Answers: [
                        { text: "Feenapfel", correct: false},
                        { text: "Mamook-Birne", correct: false},
                        { text: "O’Ghomoro-Beere", correct: false},
                        { text: "Gizar-Blatt", correct: true},
                    ]
                }
            },
            {
                reward: "50.000 Gil",
                Question: {
                    Content: "",
                    Question: "In welchem Staatsgebiet liegt der Dungeon „der versunkene Tempel von Quarn“?",
                    Answers: [
                        { text: "Ul'dah", correct: true},
                        { text: "Lavendelbeete", correct: false},
                        { text: "Kelchkuppe", correct: false},
                        { text: "Gridania", correct: false},
                    ]
                }
            },
            {
                reward: "100.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Wie heißt die Schatzmeisterin vom Bund der Morgenröte?",
                    Answers: [
                        { text: "Minfilia", correct: false},
                        { text: "Y’stola", correct: false},
                        { text: "Tataru", correct: true},
                        { text: "Miounne", correct: false},
                    ]
                }
            },
            {
                reward: "150.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Wie heißt die Abenteuergilde in Limsa Lominsa?",
                    Answers: [
                        { text: "Zur Untergegangenen Hoffnung", correct: false},
                        { text: "Zur Ertränkten Sorge", correct: true},
                        { text: "Zur Besoffenen Armut", correct: false},
                        { text: "Zum Blauen Hochmut", correct: false},
                    ]
                }
            },
            {
                reward: "200.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Woher sind die Flüchtlinge, die in Mor Dhona aufgenommen werden?",
                    Answers: [
                        { text: "Sharlayan", correct: false},
                        { text: "Ala Mhigo", correct: false},
                        { text: "Bierhafen", correct: false},
                        { text: "Doma", correct: true},
                    ]
                }
            },
            {
                reward: "250.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Wo lernt man etwas über Materia?",
                    Answers: [
                        { text: "Kakteenmulde", correct: false},
                        { text: "Der Feuerhügel", correct: true},
                        { text: "Camp Fesca", correct: false},
                        { text: "Bei Chrestchen McNiel", correct: false},
                    ]
                }
            },
            {
                reward: "350.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Was kostet eine Reise mit dem Luftschiff von Gridania nach Limsa Lominsa?",
                    Answers: [
                        { text: "80", correct: false},
                        { text: "100", correct: false},
                        { text: "120", correct: true},
                        { text: "150", correct: false},
                    ]
                }
            },
        ]
    }
    return q;
}

export function testBQuiz() : Quiz {
    let q: Quiz = {
        title: "",
        Stages: [ 
            {
                reward: "1 Gil",
                Question: {
                    Content: "",
                    Question: "Wie heißt das Wohngebiet in Ul'dah",
                    Answers: [
                        { text: "Empyrium", correct: false},
                        { text: "Kelchkuppe", correct: true},
                        { text: "Kulchgruppe", correct: false},
                        { text: "Lavendelbeete", correct: false},
                    ]
                }
            },
            {
                reward: "5.000 Gil",
                Question: {
                    Content: "",
                    Question: "In welchem Stadtstaat startet man mit der Klasse Schurke?",
                    Answers: [
                        { text: "Ul'dah", correct: false},
                        { text: "Ishgard", correct: false},
                        { text: "Limsa Lominsa", correct: true},
                        { text: "Gridania", correct: false},
                    ]
                }
            },
            {
                reward: "10.000 Gil",
                Question: {
                    Content: "",
                    Question: "Wie heißen die drei ersten Primaes?",
                    Answers: [
                        { text: "Garuda/Titan/Ifrit", correct: false},
                        { text: "Titan/Ifrit/Gardua", correct: false},
                        { text: "Iftit/Titan/Garuda", correct: true},
                        { text: "Ifrit/Titan/Mogul Mog", correct: false},
                    ]
                }
            },
            {
                reward: "25.000 Gil",
                Question: {
                    Content: "",
                    Question: "Welche Klasse verkörpert Thancred?",
                    Answers: [
                        { text: "Marodeur", correct: false},
                        { text: "Revolverklinge", correct: false},
                        { text: "Faustkämpfer", correct: false},
                        { text: "Schurke", correct: true},
                    ]
                }
            },
            {
                reward: "50.000 Gil",
                Question: {
                    Content: "",
                    Question: "Wie heißt der Chef der Gold Saucer mit Vornamen?",
                    Answers: [
                        { text: "Cid", correct: false},
                        { text: "Godbert", correct: true},
                        { text: "Manderville", correct: false},
                        { text: "Urianger", correct: false},
                    ]
                }
            },
            {
                reward: "100.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Wie heißt der Stützpunkt vom Bund der Morgenröte?",
                    Answers: [
                        { text: "Sonnenwind", correct: true},
                        { text: "Siebter Himmel", correct: false},
                        { text: "Mor Dhona", correct: false},
                        { text: "Horizontblick", correct: false},
                    ]
                }
            },
            {
                reward: "150.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Aus welchem Volk stammt Kan-E-Senna?",
                    Answers: [
                        { text: "Hyuran", correct: false},
                        { text: "Gebieter", correct: false},
                        { text: "Padjal", correct: true},
                        { text: "Ascian", correct: false},
                    ]
                }
            },
            {
                reward: "200.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Wie heißt der eiserne Admiral?",
                    Answers: [
                        { text: "Merlwyb Bloefhiswyn", correct: true},
                        { text: "Merlwib Bloefhiswin", correct: false},
                        { text: "Merlwyb Bloefhyswyn", correct: false},
                        { text: "Merlwib Blofhiswyn", correct: false},
                    ]
                }
            },
            {
                reward: "250.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Wer baut die 'Enterprise'?",
                    Answers: [
                        { text: "Gaius", correct: false},
                        { text: "Lahabrea", correct: false},
                        { text: "Cid", correct: true},
                        { text: "Urianger", correct: false},
                    ]
                }
            },
            {
                reward: "350.000 Gil",
                isHidden: true,
                Question: {
                    Content: "",
                    Question: "Steigt man in Limsa Lominsa an der Fischergilde in die Fähre, wo kann man dann überall hinfahren?",
                    Answers: [
                        { text: "Moraby-Trockendocks / Weinhafen / Dorf des Nebels", correct: false},
                        { text: "Sonnenküste / Bierhafen / Dorf des Nebels", correct: true},
                        { text: "Moraby-Trockendocks / Dorf des Nebels / Wolfshöhlenpier", correct: false},
                        { text: "Moraby-Trockendocks / Sonnenküste / Wolfshöhlenpier", correct: false},
                    ]
                }
            },
        ]
    }
    return q;
}
